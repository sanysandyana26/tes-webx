-- public.m_user definition

-- Drop table

-- DROP TABLE public.m_user;

CREATE TABLE public.m_user (
	iduser int8 NOT NULL DEFAULT nextval('m_user_seq'::regclass),
	username varchar NOT NULL,
	useremail varchar NOT NULL,
	status int8 NOT NULL,
	CONSTRAINT m_user_pk PRIMARY KEY (iduser)
);


-- public.t_register definition

-- Drop table

-- DROP TABLE public.t_register;

CREATE TABLE public.t_register (
	idregister int8 NOT NULL DEFAULT nextval('t_register_seq'::regclass),
	iduser int8 NOT NULL,
	statusregister varchar NOT NULL,
	description varchar NULL,
	dateblock timestamp NULL,
	dateunblock timestamp NULL,
	dateregister timestamp NOT NULL,
	CONSTRAINT t_register_pk PRIMARY KEY (idregister),
	CONSTRAINT t_register_fk FOREIGN KEY (iduser) REFERENCES public.m_user(iduser) ON DELETE SET NULL
);
CREATE INDEX t_register_iduser_idx ON public.t_register USING btree (iduser);
