package handler

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
	_ "github.com/lib/pq"
	"golangTest-1.1/model"
	"net/http"
	"sync"
)

var (
	lock = sync.Mutex{}
)

const (
	InteranlError  = "Internal Server Error !!"
	DataNotFount   = "Data Not Fount !!"
	MailRehistered = "Mail Registered !!"
)

func CreateUser(c echo.Context) error {
	lock.Lock()
	defer lock.Unlock()
	u := &model.User{}
	if err := c.Bind(u); err != nil {
		return err
	}
	log.Info(u.Name + " " + u.Email)

	_, i := GetUserByMail(*u)
	if i != 1 {
		return c.JSON(http.StatusOK, model.Response{Description: MailRehistered, Data: []interface{}{}})
	}

	a := InsertUser(*u)
	if a != 0 {
		return c.JSON(http.StatusOK, model.Response{Description: InteranlError, Data: []interface{}{}})
	}
	return c.JSON(http.StatusCreated, model.Response{Description: "Success", Data: []interface{}{u}})
}

func SoftDelete(c echo.Context) error {
	lock.Lock()
	defer lock.Unlock()
	u := &model.UserSoftDelete{}
	if err := c.Bind(u); err != nil {
		return err
	}
	log.Info(u.Name + " " + u.Email)

	a := softDeleteOrReRegister(*u)
	if a != 0 {
		return c.JSON(http.StatusOK, model.Response{Description: InteranlError, Data: []interface{}{}})
	}
	return c.JSON(http.StatusCreated, model.Response{Description: "Delete Success", Data: []interface{}{}})
}

func ReRegister(c echo.Context) error {
	lock.Lock()
	defer lock.Unlock()
	u := &model.UserSoftDelete{}
	if err := c.Bind(u); err != nil {
		return err
	}
	log.Info(u.Name + " " + u.Email)

	us := &model.User{
		Email: u.Email,
	}
	s, i := GetUserByMail(*us)
	if i != 0 {
		return c.JSON(http.StatusOK, model.Response{Description: MailRehistered, Data: []interface{}{}})
	}

	u.ID = s.ID
	a := softDeleteOrReRegister(*u)
	if a != 0 {
		return c.JSON(http.StatusOK, model.Response{Description: InteranlError, Data: []interface{}{}})
	}
	return c.JSON(http.StatusCreated, model.Response{Description: "Register Success", Data: []interface{}{}})
}

func GetUser(c echo.Context) error {
	lock.Lock()
	defer lock.Unlock()
	u := &model.User{}
	if err := c.Bind(u); err != nil {
		return err
	}
	log.Info(u.Name + " " + u.Email)
	a, i := GetUserByMail(*u)
	if i == 1 {
		return c.JSON(http.StatusOK, model.Response{Description: DataNotFount, Data: []interface{}{}})
	}
	return c.JSON(http.StatusOK, model.Response{Description: "Success", Data: []interface{}{a}})
}

//func UpdateUser(c echo.Context) error {
//	lock.Lock()
//	defer lock.Unlock()
//	u := new(user)
//	if err := c.Bind(u); err != nil {
//		return err
//	}
//	id, _ := strconv.Atoi(c.Param("id"))
//	users[id].Name = u.Name
//	return c.JSON(http.StatusOK, users[id])
//}

//func DeleteUser(c echo.Context) error {
//	lock.Lock()
//	defer lock.Unlock()
//	id, _ := strconv.Atoi(c.Param("id"))
//	delete(users, id)
//	return c.NoContent(http.StatusNoContent)
//}
//
func GetAllUsers(c echo.Context) error {
	lock.Lock()
	defer lock.Unlock()
	u := &model.User{}
	if err := c.Bind(u); err != nil {
		return err
	}
	log.Info(u.Name + " " + u.Email)
	a, i := GetAllUser()
	if i == 1 {
		return c.JSON(http.StatusOK, model.Response{Description: DataNotFount, Data: []interface{}{}})
	}
	return c.JSON(http.StatusOK, model.ResponseList{Description: "Success", Data: a})
}
