package handler

import (
	"fmt"
	"github.com/labstack/gommon/log"
	_ "github.com/lib/pq"
	"golangTest-1.1/midlleware"
	"golangTest-1.1/model"
)

func InsertUser(u model.User) int {
	//var id int
	db := dbConnection.CreateConnection()
	defer db.Close()

	log.Info(u.Name + " " + u.Email)
	stmt, err := db.Prepare("INSERT INTO public.m_user (username, useremail, status) VALUES ($1, $2, $3) RETURNING iduser")
	if err != nil {
		log.Fatal(err)
		return 1
	}
	defer stmt.Close()
	_, err2 := stmt.Exec(u.Name, u.Email, 1)
	if err2 != nil {
		log.Fatal(err)
		return 1
	}

	//stmt2, err := db.Prepare("INSERT INTO public.t_register (iduser, statusregister, description, dateblock, dateunblock, dateregister) VALUES( $1, '1', 'register', NULL, NULL, now());")
	//if err != nil {
	//	log.Fatal(err)
	//	return 1
	//}
	//defer stmt2.Close()
	//_, err3 := stmt.Exec(id)
	//if err3 != nil {
	//	log.Fatal(err)
	//	return 1
	//}

	fmt.Println("insert success!")

	return 0
}

func softDeleteOrReRegister(u model.UserSoftDelete) int {
	db := dbConnection.CreateConnection()
	defer db.Close()

	log.Info(u.Name + " " + u.Email)
	stmt, err := db.Prepare("UPDATE public.m_user SET status=$2 WHERE iduser=$1;")
	if err != nil {
		log.Fatal(err)
		return 1
	}
	defer stmt.Close()
	_, err2 := stmt.Exec(u.ID, u.Status)
	if err2 != nil {
		log.Fatal(err)
		return 1
	}
	fmt.Println("Soft Delete success!")

	return 0
}

func GetUserByMail(u model.User) (model.User, int) {
	db := dbConnection.CreateConnection()
	defer db.Close()

	var result = model.User{}
	err := db.
		QueryRow("SELECT iduser, username, useremail FROM public.m_user WHERE useremail= $1", u.Email).
		Scan(&result.ID, &result.Name, &result.Email)
	if err != nil {
		fmt.Println(err.Error())
		return result, 1
	}

	fmt.Printf("name: %s\n mail: %d\n", result.Name, result.Email)

	return result, 0
}

func GetAllUser() ([]model.User, int) {
	db := dbConnection.CreateConnection()
	defer db.Close()

	var result []model.User
	rows, err := db.Query("SELECT iduser, username, useremail FROM public.m_user where status = 1 ORDER BY iduser DESC ")
	if err != nil {
		fmt.Println(err.Error())
		return result, 1
	}
	defer rows.Close()

	for rows.Next() {
		var each = model.User{}
		var err = rows.Scan(&each.ID, &each.Name, &each.Email)

		if err != nil {
			fmt.Println(err.Error())
			return result, 1
		}

		result = append(result, each)
	}

	if err = rows.Err(); err != nil {
		fmt.Println(err.Error())
		return result, 1
	}

	for _, each := range result {
		fmt.Println(each.Name)
	}
	return result, 0

}
