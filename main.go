package main

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	_ "github.com/lib/pq"
	"golangTest-1.1/handler"
)

func main() {
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Routes
	e.POST("/users", handler.GetAllUsers)
	e.POST("/user/create", handler.CreateUser)
	e.POST("/user/reRegister", handler.ReRegister)
	e.POST("/user", handler.GetUser)
	e.POST("/user/Soft/delete", handler.SoftDelete)

	// Start server
	e.Logger.Fatal(e.Start(":1323"))

}
