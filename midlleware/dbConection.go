package dbConnection

import (
	"database/sql"
	"fmt"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"log"
	"os"
)

func CreateConnection() *sql.DB {
	// load .env file
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatalf("Error loading .env file")
	}

	dbinfo := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", os.Getenv("DB_HOST"), os.Getenv("DB_PORT"), os.Getenv("DB_USER"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_NAME"))
	db, err := sql.Open("postgres", dbinfo)

	if err != nil {
		log.Fatalf("Error buka koneksi " + err.Error())
	}

	// check the connection
	err = db.Ping()

	if err != nil {
		log.Fatalf("Error cek koneksi " + err.Error())
	}
	fmt.Println("Successfully connected!")

	return db
}
