package model

type User struct {
	ID    int    `json:"id"`
	Name  string `json:"name"`
	Email string `json:"email"`
}

type UserSoftDelete struct {
	ID     int    `json:"id"`
	Name   string `json:"name"`
	Email  string `json:"email"`
	Status int    `json:"status"`
}

type register struct {
	ID             int    `json:"id"`
	IdUser         string `json:"idUser"`
	StatusRegister string `json:"statusRegister"`
	Description    string `json:"description"`
	DateRegister   string `json:"dateRegister"`
	DateBlock      string `json:"dateBlock"`
	DateUnBlock    string `json:"dateUnBlock"`
}

type Response struct {
	Description string        `json:"description"`
	Data        []interface{} `json:"data"`
}
type ResponseList struct {
	Description string      `json:"description"`
	Data        interface{} `json:"data"`
}
